import requests
from bs4 import BeautifulSoup
import sqlite3
import time

header = {
"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36",
}

base_url = 'https://www.amazon.in'
url = 'https://www.amazon.in/s?k=python+book&ref=nb_sb_noss_1'

links = []

conn = sqlite3.connect('books.db')
c = conn.cursor()

c.execute('''CREATE TABLE IF NOT EXISTS books(title TEXT, url TEXT, img TEXT, author TEXT, price INT, rating INT)''')

def get_next_page(soup):
    pages = soup.find('ul', class_="a-pagination")
    if not pages.find('li', class_="a-disabled a-last"):
        url = base_url + pages.find('li', class_="a-last").findChildren('a')[0]['href']
        return url


def get_product_links(soup):
    div = soup.find_all('div', class_="s-result-item")
    for d in div:
        try:
            b_url = base_url + d.find('a', class_="a-link-normal a-text-normal")['href']
            links.append(b_url)
        except:
            continue


def parse_data(soup, url):
    try:
        title = soup.find('span', {'id': "productTitle"}).text.strip()
    except:
        title = "NA"
    try:
        auth = soup.find('span', class_="author notFaded").findChildren('a')[0].text.strip()
    except:
        auth = "NA"
    try:
        img = soup.find('div', {'id': 'img-canvas'}).findChildren('img')[0]['src']
    except:
        img = "NA"
    try:
        rating = soup.find('span', class_="a-icon-alt").text.split()[0]
    except:
        rating = "NA"

    try:
        price = soup.find('span', class_="a-size-base a-color-price a-color-price").text.strip()
    except:
        price = "NA"
    print(title, url, img, auth, price, rating)
    c.execute('INSERT INTO books VALUES(?,?,?,?,?,?)', (title, url, img, auth, price, rating))
    conn.commit()



while True:
    response = requests.get(url, headers=header)
    if response.ok:
        soup = BeautifulSoup(response.content, 'html.parser')
        url = get_next_page(soup)
        if not url:
            break
        else:
            get_product_links(soup)
    else:
        print(response)


for url in links:
    response = requests.get(url, headers=header)
    time.sleep(2)
    if response.ok:
        soup = BeautifulSoup(response.content, 'html.parser')
        parse_data(soup, url)
    else:
        print(response)

c.close()
conn.close()